import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ac-hello',
  template: `
    <h1 [style.color]="color">
      hello {{name}}!
    </h1>
  `,
})
export class HelloComponent {
  @Input() name: any = 'guest'
  @Input() color: string = 'red'
}
