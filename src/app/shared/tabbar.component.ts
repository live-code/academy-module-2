import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ac-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a
          class="nav-link "
          [ngClass]="{'active': item.id === active?.id}"
        >
          {{item.label}}
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent {
  @Input() items: any[] = [];
  @Input() active: any | null = null;
  @Output() tabClick = new EventEmitter<any>();
}
