import { Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'ac-demo-rate',
  template: `
    <h1>LIST</h1>
    <div
      class="list-group-item"
      [ngClass]="{
        highlight: odd
      }"
      *ngFor="let user of users; let i = index; let last = last; let first = first; let odd = odd; let even = even"
    >
     {{user.name}} - {{user.id}}
        <i
          *ngFor="let star of [null, null, null, null, null, null, null, null, null, null]; let i = index"
          class="fa"
          [ngClass]="{
            'fa-star': i < user.rate,
            'fa-star-o': i >= user.rate
          }"
          (click)="setRate(user, i+1)"
        ></i>
    </div>
  `,
  styles: [`
    .firstItem { border-top: 2px solid black}
    .lastItem { border-bottom: 2px dashed black}
    .highlight { background-color: #dcdcdc}
  `]
})
export class DemoRateComponent {
  users: any[] = [];

  setRate(user: any, rate: number) {
    const index = this.users.findIndex(u => u.id === user.id)
    this.users[index].rate = rate;
  }

  constructor(private http: HttpClient) {
    http.get<any[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res.map(u => ({...u, rate: u.id}));
      })
  }
}
