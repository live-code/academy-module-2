import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Required} from "../decorators/required.decorator";

@Component({
  selector: 'ac-card',
  template: `
    <div
      class="card"
      [ngClass]="{'mb-3': marginBottom}"
    >
      <div
        class="card-header bg-dark text-white"
        [ngClass]="headerCls"
        (click)="isOpened = !isOpened"
      >
        {{title}}
        <div *ngIf="icon" class="pull-right">
          <i [class]="icon"
             (click)="iconClickHandler($event)"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="isOpened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})

export class CardComponent {
  //@Required() @Input()  title: string | undefined;
  @Input()  title: string | undefined;
  @Input() headerCls: string = ''
  @Input() marginBottom: boolean = false;
  @Input() icon: string | undefined;
  @Output() iconClick = new EventEmitter();

  isOpened = false;

  iconClickHandler(event: MouseEvent) {
    event.stopPropagation()
    this.iconClick.emit()
  }
}
