import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {DemoRateComponent} from "./shared/demo-rate.component";
import { HelloComponent } from './shared/hello.component';
import { CardComponent } from './shared/card.component';
import { TabbarComponent } from './shared/tabbar.component';
import { StaticMapComponent } from './shared/static-map.component';
import { UikitDemoComponent } from './features/uikit-demo/uikit-demo.component';
import { ContactsComponent } from './features/contacts/contacts.component';
import { LoginComponent } from './features/login/login.component';
import { CatalogComponent } from './features/catalog/catalog.component';
import {RouterModule} from "@angular/router";
import { Page404Component } from './features/page404/page404.component';
import {HomeComponent} from "./features/home/home.component";
import { CatalogOffersComponent } from './features/catalog-offers/catalog-offers.component';
import { NavbarComponent } from './core/navbar.component';
import { CatalogDetailsComponent } from './features/catalog-details/catalog-details.component';
import { SettingsComponent } from './features/settings/settings.component';
import {ThemeService} from "./core/theme.service";

@NgModule({
  declarations: [
    AppComponent,
    DemoRateComponent,
    HelloComponent,
    CardComponent,
    TabbarComponent,
    StaticMapComponent,
    UikitDemoComponent,
    ContactsComponent,
    LoginComponent,
    CatalogComponent,
    Page404Component,
    CatalogOffersComponent,
    NavbarComponent,
    CatalogDetailsComponent,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'login',
        component: LoginComponent,
        // ..
      },
      { path: 'home', component: HomeComponent },
      { path: 'contacts', component: ContactsComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'catalog', component: CatalogComponent },
      { path: 'catalog/:id', component: CatalogDetailsComponent },
      { path: 'catalog/offers', component: CatalogOffersComponent },
      { path: 'uikit', component: UikitDemoComponent },
      { path: '404', component: Page404Component },
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: '**', redirectTo: '404' },
    ])
  ],
  providers: [
    ThemeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
