import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../model/user";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  fromEvent,
  interval,
  map,
  Observable,
  of,
  switchMap,
  take
} from "rxjs";

@Component({
  selector: 'ac-catalog',
  template: `
    <input type="text" #inputRef>

    <li
      *ngFor="let user of users"
      class="list-group-item"
      style="cursor: pointer"
      [routerLink]="'/catalog/' + user.id "
    >
      {{user.name}}
    </li>

  `,
})
export class CatalogComponent implements AfterViewInit{
  @ViewChild('inputRef') input!: ElementRef<HTMLInputElement>;

  users: User[] = [];
  timer: number | undefined;

  constructor(private http: HttpClient) {
    this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res;
      })
  }

  ngAfterViewInit() {
    this.input.nativeElement.focus()
    fromEvent(this.input.nativeElement, 'input')
      .pipe(
        map((ev: Event) => (ev.target as HTMLInputElement).value),
        filter(text => text.length > 3),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(name => this.http.post<User>('https://jsonplaceholder.typicode.com/users', { name }))
      )
      .subscribe(user => {
        this.users.push(user)
      })
  }
}

