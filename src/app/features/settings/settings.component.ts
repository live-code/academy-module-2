import { Component, OnInit } from '@angular/core';
import {ThemeService} from "../../core/theme.service";

@Component({
  selector: 'ac-settings',
  template: `
    <h1>THeme {{themeService.value}}</h1>
    <button (click)="themeService.value = 'dark'">Dark</button>
    <button (click)="themeService.value = 'light'">Light</button>
  `,
})
export class SettingsComponent  {

  constructor(public themeService: ThemeService) {
    console.log(themeService.value)
  }

}
