import { Component } from '@angular/core';
import {City, Country} from "./model/country";

@Component({
  selector: 'ac-uikit-demo',
  template: `
    <div class="container mt-3">

      <ac-tabbar
        [items]="countries"
        [active]="activeCountry"
        (tabClick)="selectCountry($event)"
      ></ac-tabbar>

      <ac-tabbar
        *ngIf="activeCountry"
        [items]="activeCountry.cities"
        [active]="activeCity"
        (tabClick)="selectCity($event)"
      ></ac-tabbar>

      <h1>{{activeCity?.label}}</h1>
      <ac-static-map *ngIf="activeCity" [value]="activeCity.label"></ac-static-map>


      <ac-card
        title="Profile"
        icon="fa fa-google"
        headerCls="bg-danger"
        [marginBottom]="true"
        (iconClick)="openUrl('http://www.google.com')"
      >
        <i class="fa fa-bluetooth"></i>
      </ac-card>
      <!--      <ac-card title="Chart" icon="fa fa-eye"
                     headerCls="bg-danger"

                     (iconClick)="visible = !visible">
              <div>parte 1</div>
            </ac-card>
            <div>
              <ac-card
                *ngIf="visible"
                title="alert me"
                icon="fa fa-facebook"
                headerCls="bg-danger"
                (iconClick)="doSomething()"
              >
                <form>
                  <input type="text"> <br>
                  <input type="text"> <br>
                  <input type="text"> <br>
                  <input type="text"> <br>
                </form>
              </ac-card>
            </div>-->
    </div>

  `,
})
export class UikitDemoComponent {
  visible = false;
  countries: Country[] = []
  activeCountry: Country | null = null;
  activeCity: City | null = null;

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 1,
          label: 'Japan',
          desc: 'bla bla',
          cities: [
            { id: 1, label: 'Tokyo'},
            { id: 2, label: 'Kyoto'},
          ]
        },
        {
          id: 2,
          label: 'Italy',
          desc: 'bla bla pizza',
          cities: [
            { id: 11, label: 'Rome'},
            { id: 22, label: 'Trieste'},
            { id: 23, label: 'Milano'},
          ]
        },
        {
          id: 3,
          label: 'Germany',
          desc: 'bla bla Nein!',
          cities: [
            { id: 33, label: 'Berlin'},
          ]
        }
      ]
      this.selectCountry(this.countries[0])
    }, 2000)
  }

  selectCountry(country: Country) {
    this.activeCountry = country;
    this.activeCity = this.activeCountry.cities[0]
  }

  selectCity(city: City) {
    this.activeCity = city;
  }

  openUrl(url: string) {
    window.open(url)
  }

  doSomething() {
    window.alert('ciao')
  }

}
