import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ac-page404',
  template: `
    <h1>page not found</h1>

    <img
      routerLink="/home"
      width="50" src="/assets/images/back-icon.png"
      alt="back to home">
  `,
  styles: [
  ]
})
export class Page404Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
