import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../model/user";
import {ActivatedRoute, Router} from "@angular/router";
import {catchError, interval, map, of, Subscription, switchMap, tap} from "rxjs";

@Component({
  selector: 'ac-catalog-details',
  template: `

    <div class="alert alert-danger" *ngIf="error">user does not exist</div>
    <h1>{{user?.name}}</h1>
    {{currentId}}
    <button
      *ngIf="!error"
      [routerLink]="'/catalog/' + (currentId + 1)"
      >next</button>
  `,
})
export class CatalogDetailsComponent  {
  user: User | null = null;
  currentId: number = 0;
  error: boolean = false;
  sub: Subscription;

  constructor(
    private http: HttpClient,
    private activatedRouted: ActivatedRoute,
  ) {
    this.sub = this.activatedRouted.params
      .pipe(
        map(params => +params['id']),
        switchMap(id => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`)),
      )
      .subscribe(
        res => {
          this.currentId = res.id;
          this.user = res;
        },
      )
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
