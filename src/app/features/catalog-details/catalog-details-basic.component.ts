import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../../model/user";
import {ActivatedRoute, Router} from "@angular/router";
import {catchError, interval, map, of} from "rxjs";

@Component({
  selector: 'ac-catalog-details',
  template: `

    <div class="alert alert-danger" *ngIf="error">user does not exist</div>
    <h1>{{user?.name}}</h1>
    <button
      *ngIf="!error"
      (click)="gotNextProduct()">next</button>
  `,
})
export class CatalogDetailsComponent  {
  user: User | null = null;
  currentId: number = 0;
  error: boolean = false;

  constructor(
    private http: HttpClient,
    private activatedRouted: ActivatedRoute,
    private router: Router
  ) {
    this.currentId = +activatedRouted.snapshot.params['id'];
    this.loadProduct(this.currentId);
  }

  loadProduct(id: number) {
    this.error = false;
    this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${id}`)
      .subscribe({
        next: (res) => this.user = res,
        error: () => this.error = true,
      })
  }

  gotNextProduct() {
    this.currentId = this.currentId + 1;
    this.router.navigateByUrl('/catalog/' + this.currentId)
    this.loadProduct(this.currentId)
  }
}
