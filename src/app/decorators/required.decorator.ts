export function Required(): any {
  return (target: any, propertyKey: string | symbol, propertyDescriptor: PropertyDescriptor) => {
    // console.log('here', target, propertyKey, propertyDescriptor)

    target.ngOnInit = function () {
      // console.log(this)
      if (!this[propertyKey]) {
        throw new Error(`${propertyKey.toString()} is required in ${this.constructor.name}` )
      }
    }
  }
}
