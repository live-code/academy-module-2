import { Component } from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {filter, map, tap} from "rxjs";


@Component({
  selector: 'ac-root',
  template: `
    <ac-navbar></ac-navbar>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  constructor(private router: Router) {
    router.events
      .pipe(
        filter(ev => ev instanceof NavigationEnd),
        map((ev: any) => ev.url),
      )
      .subscribe(url => {
        console.log(url)
      })
  }
}
