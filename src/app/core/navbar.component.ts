import { Component, OnInit } from '@angular/core';
import {ThemeService} from "./theme.service";
import {AuthService} from "./auth.service";

@Component({
  selector: 'ac-navbar',
  template: `
    <div
      class="p-2"
      [ngClass]="{
        'bg-dark text-white': themeService.value === 'dark',
        'bg-light': themeService.value === 'light'
      }"
    >
      <button routerLink="/login" routerLinkActive="bg-warning">Login</button>
      <button
        routerLink="/catalog"
        routerLinkActive="bg-warning"
        [routerLinkActiveOptions]="{ exact: false}"
      >Catalog</button>
      <button routerLink="/settings" routerLinkActive="bg-warning">settings</button>
      <button routerLink="/uikit" routerLinkActive="bg-warning">UIkit</button>
      <button routerLink="/contacts" routerLinkActive="bg-warning">contacts</button>
      <span>THeme {{themeService.value}}</span>
      <br>
      <span>Token {{authService.token}}</span>
    </div>
    <hr>
  `,
})
export class NavbarComponent {
  constructor(
    public themeService: ThemeService,
    public authService: AuthService
  ) {}
}
